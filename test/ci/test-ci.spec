# These variables are needed in generate_Dockerfile.sh
HEPWL_BMKEXE=test-ci-bmk.sh
HEPWL_BMKDIR=test-ci
HEPWL_BMKDESCRIPTION="DUMMY benchmark for CI tests (based on LHCb setup)"
HEPWL_BMKOS=cc7 # test the CI using cc7-base:latest (default is slc6-base:latest)

# These variables are needed in main.sh
###HEPWL_BMKOPTS="-c 1 -e 1" # DUMMY HalloWorld TEST (FASTER)
HEPWL_BMKOPTS="-c 1 -e 2" # DUMMY LHCb setup TEST (DEFAULT)
###HEPWL_BMKOPTS="-c 1 -e 3" # DUMMY LHCb Gauss setup TEST (SLOWER)
#####HEPWL_BMKOPTS="-c 200 -e 2" # Debug BMK-247 using 200 copies of the default test (DEBUG)
HEPWL_DOCKERIMAGENAME=test-ci-bmk
HEPWL_DOCKERIMAGETAG=ci0.1 # versions >= ci0.1 use common bmk driver
HEPWL_CVMFSREPOS=lhcb.cern.ch
