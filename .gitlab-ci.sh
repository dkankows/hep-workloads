#!/bin/bash

# Verbose script
#set -x

# Automatic exit on error
#set -e

# Usage
function usage() {
  echo "Usage: $(basename $0) build|test"
  exit 1
}

# Check input arguments
if [ "$1" == "build" ]; then
  stage=$1
  shift
elif [ "$1" == "test" ]; then
  stage=$1
  shift
else
  usage
fi

# Display the current date and working directory
echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\n[gitlab-ci.sh] $stage starting at $(date)\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
echo "Current directory is $(pwd)"

# Display the number of processing units available (BMK-142)
# In gitlab-runner this can be controlled via config.toml using the
# 'cpuset_cpu' parameter (NB the 'cpus' parameter has no effect instead)
# See https://stackoverflow.com/questions/54534387
# See https://serverfault.com/a/691695
echo -e "\nFrom nproc: there are $(nproc) processing units available"
echo -e "From taskset: $(taskset -c -p $$)"
echo -e "From /proc/self/status: $(cat /proc/self/status | grep Cpus_allowed_list)\n"

# Sanity check: this script assumes the gitlab CI runner's toml is configured
# with builds_dir="/scratch/builds" and volumes=["/scratch:/scratch",...], so
# that the gitlab container's $PWD can be seen by the docker-in-docker main.sh
if [ "$CI_JOB_NAME" != "noCI" ]; then
  if [ "${PWD#/scratch/builds}" == "${PWD}" ]; then echo "ERROR! Current directory is not below /scratch/builds"; exit 1; fi
fi

#
# Build stage
#
if [ "$stage" == "build" ]; then

  # Sanity check: the following CI_ variables must have been set by the gitlab CI or in run_build.sh
  for var in CI_JOB_ID CI_JOB_NAME CI_PROJECT_DIR CI_REGISTRY_IMAGE; do
    # Bash indirection ${!var}: see http://mywiki.wooledge.org/BashFAQ/006#Indirection
    if [ "${!var}" == "" ]; then echo "ERROR! $var is not set"; exit 1; fi
    echo "$var=${!var}"
  done

  # Sanity check: the value of CIENV_HEPWL_SPECFILE must have been passed in .gitlab-ci.yml or run_build.sh
  if [ "$CIENV_HEPWL_SPECFILE" == "" ]; then echo "ERROR! CIENV_HEPWL_SPECFILE is not set"; exit 1; fi
  echo -e "CIENV_HEPWL_SPECFILE=$CIENV_HEPWL_SPECFILE\n"

  # Check that the spec file exists and pass it to main.sh as a fully qualified path
  if [ ! -e $CIENV_HEPWL_SPECFILE ]; then echo "ERROR! $CIENV_HEPWL_SPECFILE does not exist"; exit 1; fi
  CIENV_HEPWL_SPECFILE=$(cd $(dirname $CIENV_HEPWL_SPECFILE); pwd)/$(basename $CIENV_HEPWL_SPECFILE)
  echo -e "Build image from spec file $CIENV_HEPWL_SPECFILE\n"

  # Set the default CIENV_BUILDEVENT if not yet set by the gitlab CI or in run_build.sh
  if [ "$CIENV_BUILDEVENT" == "" ]; then export CIENV_BUILDEVENT=all; fi
  echo "CIENV_BUILDEVENT=$CIENV_BUILDEVENT"

  # Set CIENV_MOUNTCVMFS=1 in the gitlab CI as it is not yet set
  # (conversely, CIENV_MOUNTCVMFS=0 should have been set in run_build.sh)
  if [ "$CIENV_MOUNTCVMFS" == "" ]; then
    CIENV_MOUNTCVMFS=1 # this was previously in runner's toml
  fi
  echo "CIENV_MOUNTCVMFS=$CIENV_MOUNTCVMFS"

  # Set other environment variables which were previously in the runner's toml
  # Skip image publishing and use a different naming convention in noCI mode
  if [ "$CI_JOB_NAME" != "noCI" ]; then
    CIENV_JOBDIR=/scratch/logs/CI-JOB-${CI_JOB_ID}_${CI_JOB_NAME} # previously in runner's toml
    CIENV_CVMFSVOLUME=/scratch/cvmfs_hep/CI-JOB-${CI_JOB_ID}_${CI_JOB_NAME} # previously in runner's toml
    CIENV_DOCKERREGISTRY=${CI_REGISTRY_IMAGE} # previously in runner's toml
  else
    CIENV_JOBDIR=/tmp/${USER}/logs/noCI-${CI_JOB_ID}
    CIENV_CVMFSVOLUME=/tmp/${USER}/cvmfs_hep/noCI-${CI_JOB_ID}
    CIENV_DOCKERREGISTRY=""
  fi
  echo "CIENV_JOBDIR=$CIENV_JOBDIR"
  echo "CIENV_CVMFSVOLUME=$CIENV_CVMFSVOLUME"
  echo "CIENV_DOCKERREGISTRY=$CIENV_DOCKERREGISTRY"

  # Prepare the environment for the docker-in-docker main.sh and execute it
  # NB: "docker run -v $CIENV_CVMFSVOLUME:/cvmfs:shared" would mkdir $CIENV_CVMFSVOLUME if missing, but with owner root!
  mkdir -p $CIENV_JOBDIR
  if [ "$?" != "0" ]; then echo "ERROR! 'mkdir -p $CIENV_JOBDIR' failed"; exit 1; fi
  mkdir -p $CIENV_CVMFSVOLUME
  if [ "$?" != "0" ]; then echo "ERROR! 'mkdir -p $CIENV_CVMFSVOLUME' failed"; exit 1; fi

  # Define a common singularity cache to speed up singularity runs (BMK-159)
  if [ "$CI_JOB_NAME" != "noCI" ]; then
    CIENV_SINGULARITYCACHE=/scratch/singularity/cache
  else
    if [ "$SINGULARITY_CACHEDIR" != "" ] ; then
      CIENV_SINGULARITYCACHE=$SINGULARITY_CACHEDIR
    else
      CIENV_SINGULARITYCACHE=/tmp/${USER}/singularity/cache
    fi
  fi
  echo "CIENV_SINGULARITYCACHE=$CIENV_SINGULARITYCACHE"

  # NEW OPTION 1 (BMK-145) - main.sh as a subprocess
  # These environment variables were previously passed to main.sh within docker via the file .env.file
  # They are now simply exported to the main.sh subprocess (BMK-145)
  # [NB: it is assumed here that all CI_ variables have been exported by the gitlab-runner CI]
  #export CIENV_JOBDIR
  #export CIENV_CVMFSVOLUME
  #export CIENV_MOUNTCVMFS
  #export CIENV_DOCKERREGISTRY
  #export CIENV_SINGULARITYCACHE
  #$(pwd)/build-executor/main.sh | tee -a $CIENV_JOBDIR/main.log
  #status=${PIPESTATUS[0]} # NB do not use $? if you pipe to tee...

  # OLD OPTION 2 (BMK-145) - main.sh as an entrypoint of docker run
  set | egrep -e "^(CI_|CIENV_JOBDIR|CIENV_CVMFSVOLUME|CIENV_MOUNTCVMFS|CIENV_DOCKERREGISTRY|CIENV_SINGULARITYCACHE)" | sort > $CIENV_JOBDIR/.env.file # todo: only need CIENV_CVMFSVOLUME, CIENV_JOBDIR and CIENV_SINGULARITYCACHE?
  DOCSOCK="/var/run/docker.sock"
  BUILDIMG="$CI_REGISTRY_IMAGE/hep-workload-builder:latest"
  echo "running privileged docker with fixed entrypoint"
  set -x # verbose on
  docker run --rm --privileged --entrypoint=$CI_PROJECT_DIR/build-executor/main.sh \
    --env-file $CIENV_JOBDIR/.env.file --name main_$CI_JOB_ID \
    -v $CIENV_JOBDIR:$CIENV_JOBDIR \
    -v $DOCSOCK:$DOCSOCK \
    -v $CIENV_CVMFSVOLUME:/cvmfs:shared \
    -v $CI_PROJECT_DIR:$CI_PROJECT_DIR \
    -v $CIENV_SINGULARITYCACHE:$CIENV_SINGULARITYCACHE \
    $BUILDIMG -m -s $CIENV_HEPWL_SPECFILE -e $CIENV_BUILDEVENT | tee -a $CIENV_JOBDIR/main.log
  status=${PIPESTATUS[0]} # NB do not use $? if you pipe to tee...

  set +x # verbose off
  echo -e "\n[gitlab-ci.sh] $stage back to the CI at $(date)"
  echo -e "[gitlab-ci.sh] status from main.sh: $status\n"

  # Create tarball of JOBDIR results
  echo -e "\n\n--------------\nCreating tarball of json and log results for artifacts\n--------------\n"
  if [ -e $CIENV_JOBDIR/results ];then
    find $CIENV_JOBDIR/results -name "*.json" -or -name "*.log" -or -name "log.*"  -or -name "cvmfs*.spec.txt" | tar -cvzf $CI_PROJECT_DIR/results.tar.gz -T -
  else
    echo -e "No such directory $CIENV_JOBDIR/results . Artifacts will be empty\n"
  fi
  # Clean up: remove directory CIENV_CVMFSVOLUME
  # Also unmount cvmfs and delete mount points below CIENV_CVMFSVOLUME if necessary (but main.sh
  # in docker run should have already done this and CIENV_CVMFSVOLUME should be empty by now)
  if [ "$CIENV_MOUNTCVMFS" != "0" ]; then
    echo -e "\n\n--------------\n Clean up CVMFS Volumes\n--------------\n"
    if [ ! -d ${CIENV_CVMFSVOLUME} ]; then
      echo "WARNING! Directory not found: $CIENV_CVMFSVOLUME" # e.g. symlink in /mnt, but /mnt not shared in the toml
    else
      echo "CIENV_CVMFSVOLUME is $CIENV_CVMFSVOLUME"
      if [ `ls -A $CIENV_CVMFSVOLUME | wc -l` -gt 0 ]; then
        echo "CIENV_CVMFSVOLUME directory contains:"
        ls -Al $CIENV_CVMFSVOLUME
        for mount in $(ls $CIENV_CVMFSVOLUME); do # this directory should be empty by now
          echo "WARNING! $CIENV_CVMFSVOLUME/$mount exists: try to umount and remove it"
          umount -fR $CIENV_CVMFSVOLUME/$mount || echo "WARNING! Could not umount $CIENV_CVMFSVOLUME/$mount"
          rm -rf -fR $CIENV_CVMFSVOLUME/$mount || echo "WARNING! Could not remove $CIENV_CVMFSVOLUME/$mount"
        done
      else
        echo "CIENV_CVMFSVOLUME directory is empty"
      fi
      rm -rf $CIENV_CVMFSVOLUME || echo "WARNING! Could not remove $CIENV_CVMFSVOLUME" # Remove /scratch/cvmfs_hep/CI-JOB-xxx/ top-directory
    fi
  fi

  # Clean up: clear old docker containers (BMK-122)
  echo -e "\n\n--------------\n Clean up docker containers\n--------------\n"
  $CI_PROJECT_DIR/scripts/clear_containers.sh -q

  # Clean up: clear old docker images with tag <none>
  echo -e "\n\n--------------\n Clean up docker images\n--------------\n"
  $CI_PROJECT_DIR/scripts/clear_images.sh -q

  # Clean up: clear old cvmfs mounts (BMK-122)
  # [NB This does not work: these stale mounts cannot be unmounted in the CI]
  ###echo -e "\n\n--------------\n Clean up cvmfs mounts\n--------------\n"
  ###$CI_PROJECT_DIR/scripts/clear_mounts.sh

  # Exit the CI with the return code of docker run
  echo -e "\n\nTerminating with exit status $status"
  echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  if [ $status -eq 0 ]; then
    echo -e "\n[gitlab-ci.sh] $stage finished (OK) at $(date)\n"
  else
    echo -e "\n[gitlab-ci.sh] $stage finished (NOT OK) at $(date)\n"
  fi
  echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit $status

#
# Test stage
#
elif [ "$stage" == "test" ]; then

  echo "...NOT YET IMPLEMENTED..."
  exit 1

#
# Unknown stage
#
else

  echo "INTERNAL ERROR! Unknown stage $stage"
  exit 1

fi
