export CMSSW_RELEASE=CMSSW_10_2_9
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc6_amd64_gcc700
[[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}
eval `scramv1 runtime -sh`
conddb -y copy 102X_upgrade2018_realistic_v15 --destdb GlobalTag.db
conddb -y copy L1TCaloParams_static_CMSSW_9_2_10_2017_v1_8_2_updateHFSF_v6MET --destdb GlobalTag.db
conddb -y copy L1TMuonGlobalParamsPrototype_Stage2v0_hlt --destdb GlobalTag.db
cd ..
rm -rf ${CMSSW_RELEASE}
