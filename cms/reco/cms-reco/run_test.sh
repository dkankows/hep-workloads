res=0
while [ $res ]; do
  echo "Running CMSSW..."
  OUT=`./cms-reco.sh -e 10 2> /dev/null | grep 'Parsing' | awk '{print $4}'`
  tag=`grep 'not been found' $OUT | awk -F\" '{print $2}'`
  if [ -z "$tag" ] ; then
    echo "No missing tags!"
    break
  fi
  echo "Tag $tag found to be missing!"
  ./add_tag data/GlobalTag.db $tag >> conddb.sh
  res=$?
done
