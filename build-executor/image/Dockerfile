FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Domenico Giordano <domenico.giordano@cern.ch>

LABEL multi.maintainer="Domenico Giordano <domenico.giordano@cern.ch>" \
      multi.version="0.1" \
      multi.description="docker runner to shrink cvmfs and build HEP workloads containers"

# Append 'yum clean all' to work around https://github.com/CentOS/sig-cloud-instance-images/issues/15
# But use '&& yum clean all' instead of '; yum clean all' to stop the build in case of errors!

RUN yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

RUN if [ ! -s /etc/yum.repos.d/docker-ce.repo ]; then echo "Error retrieving docker-ce.repo"; exit 1; fi

RUN yum clean all

RUN yum install -y docker-ce \
    postfix \
    rsync \
    skopeo \
    jq  \
    bc \
    ShellCheck \
    http://ecsft.cern.ch/dist/cvmfs/cvmfs-2.6.0/cvmfs-shrinkwrap-2.6.0-1.el7.x86_64.rpm \
    http://ecsft.cern.ch/dist/cvmfs/cvmfs-2.6.0/cvmfs-2.6.0-1.el7.x86_64.rpm \
    python-pip numpy \
    && yum clean all

RUN sed -e 's@inet_interfaces = localhost@#inet_interfaces = localhost@' -e 's@inet_protocols = all@inet_protocols = ipv4@' -i /etc/postfix/main.cf

# Add singularity software and configuration (BMK-77 and BMK-145)
# Merge here the singularity Dockerfile fragments developed by Chris Hollowell <hollowec@bnl.gov>
###RUN yum install -y singularity-runtime && yum clean all

# Temporarely force a specific old version 3.2.1 of Singularity to resolve BMK-253
# (latest version is 3.4.2, a fix for this issue should appear in 3.4.3)
RUN rpm --import https://repo.opensciencegrid.org/osg/3.4/RPM-GPG-KEY-OSG
RUN yum install -y https://repo.opensciencegrid.org/osg/3.4/el7/release/x86_64/singularity-3.2.1-1.1.osg34.el7.x86_64.rpm && yum clean all

# This is no longer necessary as OverlayFS is supported in CC7
###RUN sed -i -e 's/underlay = no/underlay = yes/g' /etc/singularity/singularity.conf
###RUN sed -i -e 's/overlay = try/overlay = no/g' /etc/singularity/singularity.conf

RUN pip install dictdiffer pyyaml
RUN yum install -y python34 python34-numpy  && yum clean all #Needed because there are parsers running python3

# No ENTRYPOINT
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/2109
# See https://stackoverflow.com/a/33219131
