import os
import json
import sys
import time
import re
from collections import OrderedDict

def get_number(dirname):
    """
    Checks the number of proc_* directory so that these directories can be sorted numerically.
    This fix is needed to avoid random Test-parsers pipeline failures due to different order wrt reference file.
    """
    if dirname.find('_') != -1:
        name, number = os.path.splitext(dirname)[0].split('_')
        return (name, int(number))
    else:
        return dirname

def check(infile,scriptname,appname):
    """
    Checks if run was successful. If successful returns True.
    """
    try: 
      successfulrun = False
      datafile = open(infile, 'r')
      for line in datafile:
        if 'successful run' in line:
          successfulrun = True
          break
      if successfulrun:
        return True
      else:
        return False
    except IOError:
      print ("%s ERROR (%s-bmk): File %s not found!" %(scriptname,appname,infile))
      return False

def read_values(infile):
    """
    Read information about CPU, memory and number of processed events from logfile.
    """
    nbeventslines, cpulines, cpusyslines, vmemlines, rsslines, swaplines, evtmaxcpulines, walltimelines = ([] for i in range(8))
    nbevents, cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime = ([] for i in range(9))
    with open(infile,'r') as f:
       lines = f.readlines()
       for i, line in enumerate(lines):
          if 'INFO Statistics for \'evt\'' in line:
             nbeventslines.append(line)
             cpulines.append(lines[i+1].strip())
             cpusyslines.append(lines[i+3].strip())
          if 'INFO VmPeak' in line:   
             vmemlines.append(line)
          if 'INFO VmRSS' in line:
             rsslines.append(line)
          if 'INFO VmSwap'in line:
             swaplines.append(line)
          if 'step evt' in line:
             evtmaxcpulines.append(lines[i+2].strip())
          if 'snapshot_post_fin' in line:
             walltimelines.append(line)
    for line in nbeventslines:
       nbevents.append(re.findall(r'=\s(\d+)',str(line))[0])
    for line in cpulines:
       cpuvalues = re.findall(r'(\d+).(\d{3})',str(line))
       cpuAvg.append(float(cpuvalues[0][0]+"."+cpuvalues[0][1]))
       cpuError.append(float(cpuvalues[1][0]+"."+cpuvalues[1][1]))
    for line in cpusyslines:
       cpusysvalues = re.findall(r'(\d+).(\d{3})',str(line))
       cpuSys.append(float(cpusysvalues[0][0]+"."+cpusysvalues[0][1]))
    for line in vmemlines:
       vmemPeak.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in rsslines:
       vmemRSS.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in swaplines:
       vmemSwap.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in evtmaxcpulines:
       try: 
          maxvalues = re.findall(r'(\d+)(@)(\d+)',str(line))[0]
          evtmaxcpuvalue = str(maxvalues[0])+str(maxvalues[1])+str(maxvalues[2])
          evtMaxCpu.append(evtmaxcpuvalue)
       except: 
          continue
    for line in walltimelines:
       walltime.append(re.findall(r'(\d+)',str(line))[5])
    return nbevents, cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime

def collect_values(infile,indir,appname,scriptname,wdir,step,scale,status):
    """
    Collect all information from each digi-reco step individually.
    """
    runstatus, nevt, cpus, scores, cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes = ([] for i in range(11))
    for d in indir:
        if "proc" in d:
           logfilepath = os.path.join(wdir,d,infile)
           print ("Parsing results from %s" %(logfilepath))
           if check(logfilepath,scriptname,appname):
              print ("%s INFO (%s-bmk-%s): Run was successful." %(scriptname,appname,step))
              runstatus.append(0)
           else:
              print ("%s ERROR (%s-bmk-%s): Run was not successful!" %(scriptname,appname,step))
              runstatus.append(1)
              status = 1
              continue
           nevt_proc, cpu_proc, cpuError_proc, cpuSys_proc, vmem_proc, rss_proc, swap_proc, evtmaxcpu_proc, walltime_proc = read_values(logfilepath)
           cpu_wAvg = 0.
           nevt_wAvg = 0.

           if len(cpu_proc)==len(cpuError_proc)==len(cpuSys_proc)==len(vmem_proc)==len(rss_proc)==len(swap_proc)==len(walltime_proc)==len(nevt_proc):
              for i in xrange(len(cpu_proc)):
                 if cpu_proc[i] > 0:
                    cpus.append(cpu_proc[i])
                    scores.append(scale/cpu_proc[i])
                    cpusError.append(cpuError_proc[i])
                    cpusSys.append(cpuSys_proc[i])
                    vmems.append(vmem_proc[i])
                    RSSs.append(rss_proc[i])
                    swaps.append(swap_proc[i])
                    walltimes.append(walltime_proc[i])
                    nevt.append(nevt_proc[i])
                 for i in xrange(len(evtmaxcpu_proc)):
                    evtMaxCpus.append(evtmaxcpu_proc[i])
           else:
              print ("%s ERROR (%s-bmk-%s): Number of threads is not consistent among different variables in %s!" %(scriptname,appname,step,d))
              sys.exit()
    return status, runstatus, nevt, cpus, scores, cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes

def calculate_scores(scriptname,appname,step,scores):
    """
    Calculate scores.
    """
    scores_sorted, scores_formatted, scorevalues = ([] for i in range(3))
    scores_sorted = sorted(scores)
    scores_formatted = [ '%.4f' % elem for elem in scores ]
    finalscore = 0.
    finalscore_wAvg = 0.
    avg = 0.
    median = 0.
    minimum = 0.
    maximum = 0.
    n = len(scores)
    if n > 0:
       minimum = min(scores)
       maximum = max(scores)
       for i in xrange(n):
          finalscore += scores[i]
          avg = finalscore/n
          if (n % 2 != 0):
             median = scores_sorted[(n+1)/2-1]
          else:
             median = (scores_sorted[(n/2)-1] + scores_sorted[(n/2)])/2.
       scorevalues = [ '%.4f' %(finalscore), '%.4f' %(finalscore_wAvg), '%.4f' %(avg), '%.4f' %(median), '%.4f' %(minimum), '%.4f' %(maximum) ]
    else: 
       print ("%s ERROR (%s-bmk-%s): No scores have been collected from log.%s!" %(scriptname,appname,step,step))
       sys.exit()
    return scorevalues, scores_formatted

def generate_partial_summary(statusvalue,unitvalue,values0,values1,values2,values3,values4,values5,values6,values7,values8,values9,values10):
    """
    Generate a dictionary with information from each digi-reco step.
    """
    summary = OrderedDict()
    summary["wl-stats"] = {"score":float(values1[0]), "avg":float(values1[2]), "median":float(values1[3]), "min":float(values1[4]), 
                           "max":float(values1[5])}
    summary["status"] = {"successful/all":str(len([val for val in statusvalue if val == 0]))+"/"+str(len(statusvalue)),
                         "status_copy":[val for val in statusvalue]}
    summary["wl-stats_extra"] = {"unit":unitvalue, "weighted_score":float(values1[1]), "score_per_proc":[float(val) for val in values2]}
    summary["events_per_proc_Athena(MP)"] = [int(val) for val in values0]
    summary["CPU_per_proc"] = {"cpuAvg":[val for val in values3], "cpuError":[val for val in values4],
                               "cpuSys":[val for val in values5], "evtMaxCPU":[val for val in values6]}
    summary["memory_per_proc"] = {"vmem":[val for val in values7], "RSS":[val for val in values8], "swap":[val for val in values9]}
    summary["walltime_per_proc"] = [float(val) for val in values10]
    return summary

def generate_overall_summary(values_all,json1,json2,json3,json4):
    """
    Generate a dictionary with all the information.
    """
    summary = OrderedDict()
    summary["copies"] = int(os.environ['NCOPIES'])
    summary["threads_per_copy"] = int(os.environ['NTHREADS'])
    summary["events_per_thread"] = int(os.environ['NEVENTS_THREAD'])
    wlScores = OrderedDict()
    wlScores = {"digi-reco":float(values_all[0]), "HITStoRDO":json1["wl-stats"]["score"], "RDOtoRDOTrigger":json2["wl-stats"]["score"],
                "RAWtoESD":json3["wl-stats"]["score"], "ESDtoAOD": json4["wl-stats"]["score"]}
    summary["wl-scores"] = wlScores
    stepScores = OrderedDict()
    stepScores["digi-reco"] = {"avg":float(values_all[2]), "median":float(values_all[3]), "min":float(values_all[4]), 
                         "max":float(values_all[5])}
    del json1["wl-stats"]["score"]
    stepScores["HITStoRDO"] = json1["wl-stats"]
    del json2["wl-stats"]["score"]
    stepScores["RDOtoRDOTrigger"] = json2["wl-stats"]
    del json3["wl-stats"]["score"]
    stepScores["RAWtoESD"] = json3["wl-stats"]
    del json4["wl-stats"]["score"]
    stepScores["ESDtoAOD"] = json4["wl-stats"]
    summary["wl-stats"] = stepScores
    try:
      with open(os.path.join(os.environ['BMKDIR'],"version.json")) as f:
        version_json = json.loads(f.read())
        summary["app"] = version_json
    except:
      summary["app"] = ''
    return summary

def save_output(data,appname,scriptname,outfile):
    """
    Save output to a JSON file.
    """
    jsonfile = json.dumps(data)
    fjson = open(outfile,"w")
    fjson.write(jsonfile)
    fjson.close()
    print ("%s INFO (%s-bmk): Summary placed in %s" %(scriptname,appname,outfile))

def main():
    """
    Main function where we parse results from a logfile and save them to JSON. 
    """

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    pythonstatus = 0

    # Environment variables
    basename     = os.path.basename(__file__)
    basewdir     = os.environ['BASE_WDIR']
    bmkdir       = os.environ['BMKDIR']
    app          = os.environ['APP']
    overallJSON  = os.path.join(basewdir,app+"_summary.json")

    # Global variables
    logfiles = ["log.HITtoRDO", "log.RDOtoRDOTrigger", "log.RAWtoESD", "log.ESDtoAOD"]
    scale    = 1000
    if scale == 1:
       unit = "evt/ms"
    elif scale == 1000:   
       unit = "evt/s"
    elif scale == 1000000:
       unit = "evt/ks"
    else:
      unit = ""
      print ("%s WARNING (%s-bmk): Scale %i does not have predefined unit! Please define it." %(basename,app,scale))

    # Find logfiles
    dirs = []
    for (dirpath, dirnames, filenames) in os.walk(basewdir):
        dirs.extend(dirnames)
        break

    ##################
    # Digi-reco steps
    #################
    # Parse results from logfiles and calculate scores for each digi-reco step
    cpus_HITtoRDO, cpus_RDOtoRDOTrigger, cpus_RAWtoESD, cpus_ESDtoAOD = ([] for i in range(4))
    jsonSummary_HITtoRDO, jsonSummary_RDOtoRDOTrigger, jsonSummary_RAWtoESD, jsonSummary_ESDtoAOD = ({} for i in range(4))
    for logfile in logfiles:
       recostep = logfile.split(".")[1]
       runstatus_step, nevt_step, cpus_step, cpusError_step, cpusSys_step, evtMaxCpus_step, vmems_step, RSSs_step, swaps_step, walltimes_step = ([] for i in range(10))
       pythonstatus, runstatus_step, nevt_step, cpus_step, scores_step, cpusError_step, cpusSys_step, evtMaxCpus_step, vmems_step, RSSs_step, swaps_step, walltimes_step = collect_values(logfile,sorted(dirs, key=get_number),app,basename,basewdir,recostep,scale,pythonstatus)

       scorevalues_step, scores_formatted_step = calculate_scores(basename,app,recostep,scores_step)

       # Generate summary and save to a JSON file
       partialJSON  = os.path.join(basewdir,recostep+"-"+app+"_summary.json")
       jsonSummary_step = generate_partial_summary(runstatus_step, unit, nevt_step, scorevalues_step, scores_formatted_step, cpus_step, 
                                                   cpusError_step, cpusSys_step, evtMaxCpus_step, vmems_step, RSSs_step, swaps_step, walltimes_step)
       jsonPartialSummary = {} 
       jsonPartialSummary[recostep] = jsonSummary_step
       save_output(jsonPartialSummary,app,basename,partialJSON)
       #print jsonSummary_step   
       #print jsonPartialSummary

       # Check if partial JSON file was created 
       if not os.path.isfile(partialJSON):
          print ("%s ERROR (%s-bmk): Something went wrong in parsing the CPU score. File path %s does not exist!" %(basename,app,partialJSON))
          pythonstatus = 1

       # Save for futher use 
       if recostep == "HITtoRDO": 
          cpus_HITtoRDO = cpus_step
          jsonSummary_HITtoRDO = jsonSummary_step
       elif recostep == "RDOtoRDOTrigger": 
          cpus_RDOtoRDOTrigger = cpus_step
          jsonSummary_RDOtoRDOTrigger = jsonSummary_step
       elif recostep == "RAWtoESD": 
          cpus_RAWtoESD = cpus_step
          jsonSummary_RAWtoESD = jsonSummary_step
       elif recostep == "ESDtoAOD": 
          cpus_ESDtoAOD = cpus_step
          jsonSummary_ESDtoAOD = jsonSummary_step

    #########
    # Total
    ##########
    # Caclulate total score   
    scores_all = []
    if len(cpus_HITtoRDO) == len(cpus_RDOtoRDOTrigger) == len(cpus_RAWtoESD) == len(cpus_ESDtoAOD):
       for i in xrange(len(cpus_HITtoRDO)):
          scores_all.append(scale/(cpus_HITtoRDO[i]+cpus_RDOtoRDOTrigger[i]+cpus_RAWtoESD[i]+cpus_ESDtoAOD[i]))
    else: 
       pythonstatus = 1

    scorevalues_all, scores_formatted_all = calculate_scores(basename,app,"all",scores_all)

    # Generate summary and save to a JSON file
    totalJSON  = os.path.join(basewdir,app+"_summary.json")
    jsonSummary_all = generate_overall_summary(scorevalues_all,jsonSummary_HITtoRDO,jsonSummary_RDOtoRDOTrigger,jsonSummary_RAWtoESD,jsonSummary_ESDtoAOD)
    jsonOverallSummary = {}
    jsonOverallSummary["wl-stats"] = jsonSummary_HITtoRDO

    save_output(jsonSummary_all,app,basename,totalJSON)

    # Check if total JSON file was created 
    if not os.path.isfile(totalJSON):
       print ("%s ERROR (%s-bmk): Something went wrong in parsing the CPU score. File path %s does not exist!" %(basename,app,totalJSON))
       pythonstatus = 1

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    sys.exit(pythonstatus)

if '__main__' in __name__:
    main()
