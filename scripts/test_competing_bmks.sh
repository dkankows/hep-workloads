#!/bin/sh

function dump_fluentd_conf(){

cat > fluent.conf << EOF

<source>
  @type  forward
  @id    input1
  @label @mainstream
  port  24224
</source>

<filter **>
  @type stdout
</filter>

<label @mainstream>
  <match docker.**>
    @type file
    @id   output_docker1
    path         /fluentd/log/docker.*.log
    symlink_path /fluentd/log/docker.log
    append       true
    time_slice_format %Y%m%d
    time_slice_wait   1m
    time_format       %Y%m%dT%H%M%S%z
    format json
    include_time_key true
  </match>
  <match **>
    @type file
    @id   output1
    path         /fluentd/log/data.*.log
    symlink_path /fluentd/log/data.log
    append       true
    time_slice_format %Y%m%d
    time_slice_wait   10m
    time_format       %Y-%m-%dT%H:%M:%S%z
    utc
    format json
    include_time_key true
  </match>
</label>

EOF
}

function TestCase(){

    NCPUS=$1
    T_CPUS=$(($NCPUS-1))
    echo T_CPUS $T_CPUS

    for in in `seq 1 5`;
    do
	docker run --rm  --name=db12cpp --log-driver=fluentd --cpuset-cpus=0-${T_CPUS} gitlab-registry.cern.ch/giordano/db12-cpp/db12cpp:test -n$NCPUS
    done

    for in in `seq 1 5`;
    do
	docker run --rm  --name=db12 --log-driver=fluentd --cpuset-cpus=0-${T_CPUS} gitlab-registry.cern.ch/giordano/db12-cpp/db12:test $NCPUS
    done


    for in in `seq 1 2`;
    do
	docker run --rm  --name=kv --log-driver=fluentd --cpuset-cpus=0-${T_CPUS} -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/atlas-kv-bmk:latest -n$NCPUS
    done


    if [ $NCPUS -eq 8 ]; 
	then
	docker run -d --rm --name=cms --log-driver=fluentd  --cpuset-cpus=0-6 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t7
    elif [ $NCPUS -eq 12 ];
	then
	docker run -d --rm --name=cms1 --log-driver=fluentd  --cpuset-cpus=0-4 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t5
	docker run -d --rm --name=cms2 --log-driver=fluentd  --cpuset-cpus=5-11 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t6
    elif [ $NCPUS -eq 16 ];
	then
	docker run -d --rm --name=cms1 --log-driver=fluentd  --cpuset-cpus=0-4 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t5
	docker run -d --rm --name=cms2 --log-driver=fluentd  --cpuset-cpus=5-9 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t5
	docker run -d --rm --name=cms3 --log-driver=fluentd  --cpuset-cpus=10-14 -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/cms-gen-sim:latest -d -t5
    else
	echo "DEFINE a proper modularity"
	exit 1
    fi 
    sleep 10m

    for in in `seq 1 5`;
    do
	docker run --rm  --name=db12cpp --log-driver=fluentd --cpuset-cpus=$T_CPUS gitlab-registry.cern.ch/giordano/db12-cpp/db12cpp:test -n1
    done

    for in in `seq 1 5`;
    do
	docker run --rm  --name=db12 --log-driver=fluentd --cpuset-cpus=$T_CPUS gitlab-registry.cern.ch/giordano/db12-cpp/db12:test 1
    done

    for in in `seq 1 5`;
    do
	docker run --rm  --name=kv --log-driver=fluentd --cpuset-cpus=$T_CPUS  -v `pwd`/results_single_bmk:/results gitlab-registry.cern.ch/giordano/hep-workloads/atlas-kv-bmk:latest
    done

    docker ps -a | grep cms | cut -d " " -f1 | xargs -n1 docker rm -f
}

cd ~
dump_fluentd_conf
mkdir results_single_bmk
docker run -d --name=fluent_doc -p 24224:24224 -p 24224:24224/udp -v `pwd`/results_single_bmk:/fluentd/log -v `pwd`:/fluentd/etc fluent/fluentd

TestCase 8
TestCase 12
TestCase 16
