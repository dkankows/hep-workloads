#!/bin/bash

set -e # immediate exit on error

scrdir=$(cd $(dirname $0); pwd)
bmkspecs=$(cd $scrdir/..; ls */*/*.spec)
for bmkspec in $bmkspecs; do
  bmknam=$(basename $bmkspec .spec)
  echo -e "\n=============================================================\n${bmknam}"
  echo -e "=============================================================\n"
  bmkdir=$(dirname $bmkspec)/$bmknam
  if [ ! -d $bmkdir/jobs ]; then continue; fi
  ###jobnams=$(ls -1 $bmkdir/jobs | grep -v refjob)
  jobnams=refjob
  for jobnam in $jobnams; do
    echo -e "\n+++$bmkdir/jobs/$jobnam+++\n"
    if [ -f $bmkdir/jobs/$jobnam/joburl.txt ]; then
      joburl=$(cat $bmkdir/jobs/$jobnam/joburl.txt)
      $scrdir/referenceFromJobUrl.sh -n $jobnam $joburl -d   
    else
      echo "WARNING! joburl.txt not found"
    fi
  done
done
